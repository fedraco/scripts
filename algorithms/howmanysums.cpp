#include<iostream>
#include<vector>
#include<algorithm>
#include<iterator>
#include<map>
#include<unordered_map>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::unordered_map;
using std::map;
using std::pair;

long int howmanysums(int sum, std::size_t adjusted_component_length, const vector<int>& components, map<pair<int,int>,long int>& storage);
long int howmanysums(int sum, const vector<int>& components);

std::istream& operator>>(std::istream& in, vector<int>& to);

int main() {
  int sum;
  vector<int> components;
  cout << "Input sum " << endl;
  cin >> sum;
  cout << "Input the components and then press Ctrl+D: " << endl;
  cin >> components;
  cout << "The number of possible unique sums is " << howmanysums(sum, components) << endl;
  return 0;
}

std::istream& operator>>(std::istream& in, vector<int>& to) {
  std::copy(std::istream_iterator<int>(in), std::istream_iterator<int>(), std::back_inserter(to));
  return in;
}

long int howmanysums(int sum, int current_component, const vector<int>& components, map<pair<int,int>,long int>& storage) {
  if (sum == 0) {
    return 1;
  } else if (sum < 0 || current_component < 0) {
    return 0;
  } else if (auto stored = storage.find({sum, current_component}); stored != storage.end()) {
    return stored->second;
  } else {
    long int solution = howmanysums(sum - components[current_component], current_component, components, storage) + howmanysums(sum, current_component - 1, components, storage);
    storage.insert({{sum, current_component}, solution});
    return solution;
  }
}

long int howmanysums(int sum, const vector<int>& components) {
  map<pair<int,int>, long int> storage;
  int current_component = components.size() - 1;
  return howmanysums(sum, current_component, components, storage);
}
