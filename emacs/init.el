;;; init.el --- Initialization file for Emacs
;;; Commentary:
;;; Emacs start-up file
;;; Code:

;;; Elpaca

(defvar elpaca-installer-version 0.10)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil :depth 1 :inherit ignore
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (<= emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let* ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                  ((zerop (apply #'call-process `("git" nil ,buffer t "clone"
                                                  ,@(when-let* ((depth (plist-get order :depth)))
                                                      (list (format "--depth=%d" depth) "--no-single-branch"))
                                                  ,(plist-get order :repo) ,repo))))
                  ((zerop (call-process "git" nil buffer t "checkout"
                                        (or (plist-get order :ref) "--"))))
                  (emacs (concat invocation-directory invocation-name))
                  ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                        "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                  ((require 'elpaca))
                  ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

(elpaca elpaca-use-package (elpaca-use-package-mode))

(defun my/newline-after-end-of-line ()
  "Move point to end of current line and insert a newline."
  (interactive)
  (move-end-of-line nil)
  (newline))

;;; Global settings

(use-package emacs
  :ensure nil
  :init
  (load "~/.emacs.d/custom.el")
  (load "~/.emacs.d/personal-settings.el")
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1)
  (pixel-scroll-precision-mode)
  :custom
  (native-comp-async-report-warnings-errors 'silent)
  (inhibit-startup-screen t)
  (column-number-mode t)
  (visible-bell t)
  (large-file-warning-threshold nil)
  (delete-by-moving-to-trash t)
  (load-prefer-newer t)
  (indent-tabs-mode nil)
  (desktop-auto-save-timeout nil)
  :bind
  (("M-p" . scroll-up-line)
   ("M-n" . scroll-down-line)
   ("M-k" . (lambda () (interactive) (kill-line 0)))
   ("S-<return>" . my/newline-after-end-of-line)
   ("C-." . delete-all-space))
  :hook
  (elpaca-after-init .
                     (lambda ()
                       (desktop-save-mode 1)
                       (desktop-read))))

;;; Theme

(use-package modus-themes
  :ensure t
  :config
  (load-theme 'modus-operandi))

;;; Global modes

(use-package diminish
  :ensure t)

(use-package which-key
  :ensure t
  :diminish
  :init
  (which-key-mode))

(use-package dired-x
  :ensure nil
  :after dired)

(use-package dired-du
  :ensure t
  :after dired)

(use-package emojify
  :ensure t
  :hook
  (org-mode . emojify-mode)
  (emojify-list-mode . emojify-mode))

(use-package ace-window
  :ensure t
  :bind (("M-o" . ace-window)
	 ("M-O" . ace-swap-window)))

(use-package helm
  :ensure t
  :demand
  :diminish
  :config
  (helm-mode 1)
  :bind (([remap execute-extended-command] . helm-M-x)
	 ([remap bookmark-jump] . helm-filtered-bookmarks)
	 ([remap find-file] . helm-find-files)
	 ([remap switch-to-buffer] . helm-mini)
	 ([remap occur] . helm-occur))
  :custom
  (helm-findutils-search-full-path t))

(use-package helm-rg
  :ensure t
  :demand
  :after (helm))

(use-package mise
  :ensure t
  :init
  (global-mise-mode))

(use-package projectile
  :ensure t
  :diminish projectile-mode
  :init
  (when (file-directory-p my/local-project-folder)
    (setq projectile-project-search-path `((,my/local-project-folder . 2))))
  (projectile-mode +1)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :custom
  (projectile-switch-project-action #'projectile-dired)
  (projectile-enable-cmake-presets t))

(use-package helm-projectile
  :ensure t
  :init
  (helm-projectile-on)
  :bind (:map projectile-command-map
	      ("h" . helm-projectile)))

(use-package line-reminder
  :ensure t
  :config
  (global-line-reminder-mode t)
  :custom
  (line-reminder-show-option 'indicators))

(use-package avy
  :ensure t
  :bind
  (("C-:" . avy-goto-char)
   ("C-'" . avy-goto-char-2)
   ("M-g t" . avy-goto-char-timer))
  )

(use-package company
  :ensure t
  :diminish
  :demand t
  :bind
  (:map company-active-map
	("<return>" . nil)
	("RET" . nil)
					; This will make sure hitting return to insert a new line won't select the suggestion
	("C-SPC" . 'company-complete-selection))
  :config
  (setq company-idle-delay 0.3)
  (global-company-mode t))

(use-package flycheck
  :ensure t
  :diminish
  :init (global-flycheck-mode))

(use-package terminal-here
  :ensure t
  :bind (("C-<f5>" . #'terminal-here-launch)
	 ("C-<f6>" . #'terminal-here-project-launch)))

;;; Magit

(use-package transient
  :ensure t)

(use-package magit
  :ensure t
  :bind
  (("C-x g" . magit-status))
  :custom
  (magit-diff-refine-hunk 'all))

;;; Treemacs

(use-package treemacs
  :ensure t
  :config
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-eldoc-display                   'simple
          treemacs-file-event-delay                2000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-find-workspace-method           'find-for-file-or-pick-first
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-header-scroll-indicators        '(nil . "^^^^^^")
          treemacs-hide-dot-git-directory          t
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-persist-file                    (expand-file-name
						    ".cache/treemacs-persist"
						    user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-project-follow-into-home        nil
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)
    (when treemacs-python-executable
      (treemacs-git-commit-diff-mode t))

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("C-x M-0"   . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t d"   . treemacs-select-directory)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))


(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

;;; vterm

(defun my/iproc ()
  (interactive)
  (let ((process (get-buffer-process (current-buffer))))
    (if process
        (shell-command (format "pstree %d" (process-id process)))
      (message "No process"))))

(defun my/process-has-subprocesses (process &optional print-message)
  "Determine if PROCESS has a subprocess.
Display message in the echo area if PRINT-MESSAGE is true.
If used interactively, the process from the current buffer
is taken into account."
  (interactive (list (get-buffer-process (current-buffer)) t))
  (if process
      (if (process-running-child-p process)
	  (progn
	    (when print-message
	      (message "t"))
	    t)
	(when print-message
	  (message "nil"))
	nil)
    (when print-message
      (message "No process"))
    nil))

(use-package vterm
  :ensure t
  :init
  (defun my/no-process-query-on-exit-if-no-subprocesses ()
    (let ((proc (get-buffer-process (current-buffer))))
      (if (and (processp proc) (not (my/process-has-subprocesses proc)))
	  t
	(process-kill-buffer-query-function))))
  (defun my/add-local-process-query-hook ()
    (add-hook 'kill-buffer-query-functions 'my/no-process-query-on-exit-if-no-subprocesses nil t)
    (remove-hook 'kill-buffer-query-functions 't t))
  (define-advice save-buffers-kill-emacs
      (:around (orig-foo &rest args) my/unset-process-query-on-exit-flag-if-no-subprocesses)
    ;; Set process-query-on-exit-flag to nil for each
    ;; vterm process which does not currently have any
    ;; subprocess running.
    (interactive "P")
    (let ((changed-procs)
	  (orig-flags))
      (when (boundp 'multi-vterm-buffer-list)
	(dolist (buffer multi-vterm-buffer-list)
	  (let ((proc (get-buffer-process buffer)))
	    (when (and (processp proc) (not (my/process-has-subprocesses proc)))
	      ;; Store original flags and respective processes to be restored in case Emacs is
	      ;; not killed
	      (push proc changed-procs)
	      (push (process-query-on-exit-flag proc) orig-flags)
	      (set-process-query-on-exit-flag proc nil)))))
      (let ((res (apply orig-foo args)))
	;; If Emacs was not killed, restore original flags
	(when (not res)
	  (let ((changed-procs-orig-flags (-zip changed-procs orig-flags)))
	    (dolist (proc-and-flag changed-procs-orig-flags)
	      (set-process-query-on-exit-flag (car proc-and-flag) (cdr proc-and-flag))))))))
  :hook
  (vterm-mode . my/add-local-process-query-hook)
  :custom
  (vterm-shell "/usr/bin/fish")
  (vterm-kill-buffer-on-exit t)
  (vterm-timer-delay 0.05)
  )

(use-package multi-vterm
  :ensure t
  :bind
  (("C-x v e" . multi-vterm))
  )

;;; Org mode

(defvar my/calendar-file
  (file-name-concat my/org-directory "diary.org")
  "Personal calendar org file.")

(defun my/org-capture-create-id ()
  (when (org-capture-get :create-id)
    (org-id-get-create)))

(defun my/org-mode-buffer-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

(use-package org
  :ensure t
  :mode (("\\.org" . org-mode))
  :init
  (require 'org-capture)
  (add-hook '
   org-capture-mode-hook #'my/org-capture-create-id)
  (add-hook 'org-mode-hook #'my/org-mode-buffer-setup)
  :config
  (add-to-list 'org-structure-template-alist
	       '("sj" . "SRC jupyter-python :session main :async yes :pandoc t"))
  ;; "C-'" is already used by avy
  (unbind-key (kbd "C-'") org-mode-map)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (jupyter . t)))
  :custom
  ;; Esthetics
  (org-src-preserve-indentation t) ;; Fix strange leading indentation inside src blocks
  (org-special-ctrl-a/e 'reversed)
  (org-ellipsis " ▼")
  (org-hide-emphasis-markers t)
  ;; Behavior
  (org-archive-location (file-name-concat my/org-directory  "todos_archive.org::"))
  (org-default-notes-file (file-name-concat my/org-directory  "inbox.org"))
  (org-agenda-diary-file my/calendar-file)
  (org-agenda-files
   (list
    (file-name-concat my/org-directory "inbox.org")
    (file-name-concat my/org-directory "todos.org")
    (file-name-concat my/org-directory "projects")
    (file-name-concat my/org-directory "root")
    my/calendar-file)
   )
  (org-use-property-inheritance '("KIND"))
  (org-refile-targets '((nil :maxlevel . 9)
			(org-agenda-files :maxlevel . 9)))
  (org-outline-path-complete-in-steps nil)
  (org-refile-use-outline-path 'file)
  (org-todo-keywords
   '((sequence "TODO" "INPROGRESS" "BLOCKED" "|" "DONE" "NOFIX")))
  (org-priority-lowest ?D)
  (org-priority-default ?D)
  (org-capture-templates
   '(("t" "Todo" entry (file+headline "" "Todos")
      "* TODO %?"
      :create-id t
      :kill-buffer t)
     ("a" "Calendar entry" entry (file+olp+datetree org-agenda-diary-file)
      "* %?\n%t"
      :create-id nil
      :kill-buffer t)
     ("l" "Link bookmark" entry
      (file+headline my/org-links-dir "Refile")  ; previously file+function
      ;; (lambda ()
      ;;   (let ((prompt "Capture location"))
      ;; 	 (org-refile t nil nil prompt))))
      "* %^L\n%T\n%?"
      :kill-buffer t)))
  (org-confirm-babel-evaluate nil)
  (org-refile-allow-creating-parent-nodes 'confirm)
  (org-agenda-custom-commands
   '(
     ("l" search "+{\\[\\[} +{\\]\\[} +{\\]\\]}"
      ((org-agenda-files `(,my/org-links-dir))))
     ("k" tags "+KIND=\"kb\"")))
  :custom-face
  (org-block ((t (:foreground nil :inherit (shadow fixed-pitch)))))
  (org-code ((t (:inherit (shadow fixed-pitch)))))
  (org-table ((t (:inherit (shadow fixed-pitch)))))
  (org-verbatim ((t (:inherit (shadow fixed-pitch)))))
  (org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
  (org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
  (org-checkbox ((t (:inherit fixed-pitch))))
  (org-document-title ((t (:height 2.5))))
  (line-number ((t (:inherit (shadow fixed-pitch)))))
  (org-level-1 ((t (:height 1.5))))
  (org-level-2 ((t (:height 1.1))))
  (org-level-3 ((t (:height 1.05))))
  (org-level-4 ((t (:height 1.0))))
  (org-level-5 ((t (:height 1.1))))
  (org-level-6 ((t (:height 1.1))))
  (org-level-7 ((t (:height 1.1))))
  (org-level-8 ((t (:height 1.1))))
  :bind
  (("C-c a" . org-agenda)
   ("C-c c" . helm-org-capture-templates))
  )

(use-package helm-org
  :ensure t
  :init
					; Helm Org tag completion will work only if you disable org fast tag selection
  (setq org-use-fast-tag-selection nil)
  (add-to-list
   'helm-completing-read-handlers-alist '(org-capture . helm-org-completing-read-tags))
  (add-to-list
   'helm-completing-read-handlers-alist '(org-set-tags . helm-org-completing-read-tags))
  :custom
  (helm-org-ignore-autosaves t))

;;; File modes

(use-package csv-mode
  :ensure t)

(use-package markdown-mode
  :ensure t
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package auctex
  :ensure (auctex :pre-build (("./autogen.sh")
			      ("./configure"
			       "--without-texmf-dir"
			       "--with-packagelispdir=./"
			       "--with-packagedatadir=./")
			      ("make"))
		  :build (:not elpaca--compile-info) ;; Make will take care of this step
		  :files ("*.el" "doc/*.info*" "etc" "images" "latex" "style")
		  :version (lambda (_) (require 'tex-site) AUCTeX-version))
  :mode (("\\.tex\\'" . TeX-latex-mode)
	 ("\\.sty\\'" . TeX-latex-mode))
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t))

(use-package pdf-tools
  :ensure t
  :mode  ("\\.pdf\\'" . pdf-view-mode)
  :config
  (pdf-loader-install))

;;; Misc modes

(use-package elfeed
  :ensure t
  :defer t
  :bind (("C-x w" . 'elfeed))
  :custom (elfeed-search-title-max-width 100))

(use-package elfeed-org
  :ensure t
  :after (elfeed)
  :config (elfeed-org)
  :custom (rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org")))

;;; Programming

(defun my/prog-mode-settings-function ()
  "Enable features useful when programming."
  (electric-pair-mode)
  (show-paren-mode)
  (display-line-numbers-mode)
  (hl-line-mode))

(add-hook 'prog-mode-hook 'my/prog-mode-settings-function)

(use-package treesit-auto
  :ensure t
  :custom
  (treesit-auto-install 'prompt)
  :config
  (setq my-zig-tsauto-config
        (make-treesit-auto-recipe
         :lang 'zig
         :ts-mode 'zig-ts-mode
         :url "https://github.com/tree-sitter-grammars/tree-sitter-zig"
         :revision "v1.1.2"
         :source-dir "src"
         :ext "\\.zig\\'"))
  (add-to-list 'treesit-auto-recipe-list my-zig-tsauto-config)
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))


(use-package eldoc
  :diminish)

(use-package apheleia
  :ensure t
  :hook
  (prog-mode . apheleia-mode))

(use-package mhtml-mode
  :ensure nil
  :config
  (keymap-set html-mode-map "C-c M-o" facemenu-keymap)
  (keymap-unset html-mode-map "M-o"))

(use-package jupyter
  :ensure t
  )

(use-package haskell-mode
  :ensure t)

(use-package lsp-haskell
  :ensure t)

(defun my/c-ts-indent-style ()
  "Personal indentation style based on K&R."
  `(
    ((n-p-gp nil "declaration_list" "namespace_definition") grand-parent 0)
    ((node-is "access_specifier") grand-parent 0)
    ,@(alist-get 'k&r (c-ts-mode--indent-styles 'cpp))))

(defun my/lsp-c++-install-save-hooks ()
  "Install LSP save hooks for C++."
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))

(use-package c-ts-mode
  :ensure nil
  :custom
  (c-ts-mode-indent-offset 4)
  (c-ts-mode-indent-style #'my/c-ts-indent-style)
  :hook
  ((c++-ts-mode . (lambda () (electric-indent-local-mode -1)))
   (c++-ts-mode . my/lsp-c++-install-save-hooks)))

(use-package cc-mode
  :ensure nil
  :custom
  (c-basic-offset 4))

(use-package zig-mode
  :ensure t)

(use-package lsp-mode
  :ensure t
  :init
  (setq-default lsp-use-plists "true")
  (setq-default gc-cons-threshold 100000000)
  (setq-default read-process-output-max (* 1024 1024))
  :hook
  ((lsp-mode . lsp-enable-which-key-integration)
   (c++-ts-mode . lsp-deferred)
   (c-ts-mode . lsp-deferred)
   (python-ts-mode . lsp-deferred)
   (haskell-mode . lsp-deferred)
   (haskell-literate-mode . lsp-deferred)
   (zig-mode . lsp-deferred))
  :bind-keymap
  ("C-c l" . lsp-command-map)
  :custom
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-pylsp-plugins-rope-completion-enabled t))

(use-package lsp-treemacs
  :ensure t
  :after (lsp-mode)
  :config
  (lsp-treemacs-sync-mode 1))

(use-package cmake-ts-mode
  :ensure nil
  :hook (cmake-ts-mode . lsp-deferred)
  :custom
  (cmake-ts-mode-indent-offset 4))

(use-package cmake-font-lock
  :ensure t
  :after cmake-mode
  :config (cmake-font-lock-activate))

(use-package lsp-ui
  :ensure t
  :after (lsp-mode)
  :commands lsp-ui-mode
  :hook
  (lsp-mode . lsp-ui-mode))

(use-package helm-lsp
  :ensure t
  :after (lsp-mode)
  :commands helm-lsp-workspace-symbol)

(use-package dap-mode
  :ensure t
  :after lsp-mode
  :config
  (dap-auto-configure-mode)
  (require 'dap-cpptools)
  :custom
  (dap-internal-terminal #'dap-internal-terminal-vterm)
  )

;;; init.el ends here
;;; Local Variables:
;;; eval: (outline-minor-mode)
;;; End:
