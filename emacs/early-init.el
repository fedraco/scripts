;;; early-init.el --- Initialization file for Emacs
;;; Commentary:
;;; Emacs start-up file
;;; Code:


(setq package-enable-at-startup nil)

;; Use plist when communicating with language servers
(setenv "LSP_USE_PLISTS" "1")

;;; early-init.el ends here
